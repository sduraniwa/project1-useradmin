-- Dumping UserAdmin database structure for users_db

CREATE DATABASE IF NOT EXISTS users_db /*!40100 DEFAULT CHARACTER SET latin1 */;
USE users_db;

DROP TABLE IF EXISTS user;
-- Dumping structure for table users_db.user
CREATE TABLE IF NOT EXISTS user (
	id bigint(20) NOT NULL  AUTO_INCREMENT,
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL, 
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	email varchar(255) NOT NULL, 
    state VARCHAR(255) NOT NULL,  
	PRIMARY KEY (id),
	UNIQUE(username)
)ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS profile;
-- Dumping structure for table users_db.profile
CREATE TABLE IF NOT EXISTS profile (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  key_type varchar(255) NOT NULL,
  name_type varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE(key_type)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS user_profile;
-- Dumping structure for table users_db.user_profile
CREATE TABLE IF NOT EXISTS user_profile (
  user_id bigint(20) NOT NULL,
  profile_id bigint(20) NOT NULL,
  PRIMARY KEY(user_id,profile_id),
  CONSTRAINT FK_USER FOREIGN KEY (user_id) REFERENCES user (id),
  CONSTRAINT FK_PROFILE FOREIGN KEY (profile_id) REFERENCES profile (id)
  
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Populate data for table users_db.profile: ~3rows
/*!40000 ALTER TABLE profile DISABLE KEYS */;
INSERT INTO PROFILE(key_type, name_type)
VALUES ('TEST','Tester');
  
INSERT INTO PROFILE(key_type, name_type)
VALUES ('ADMI','Administrator');
  
INSERT INTO PROFILE(key_type, name_type)
VALUES ('DEVE', 'Developer');

INSERT INTO PROFILE(key_type, name_type)
VALUES ('USER', 'User');

-- Populate USER Table --
/*!40000 ALTER TABLE user DISABLE KEYS */;
INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('garellano','1234', 'Gerardo','Arellano','garellano@xyz.com', 'Active');
 
INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('cperez','1234', 'Carlos','Perez','cperez@xyz.com', 'Active');
 
INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('jduran','1234', 'Julio','Duran','jduran@xyz.com', 'Active');
 
INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('sduran','1234', 'Sergio','Duran','sduran@xyz.com', 'Active');
 
INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('agutierrez','1234', 'Axel','Gutierrez','agutierrez@xyz.com', 'Active');

INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('fperez','1234', 'Francisco','Perez','fperez@xyz.com', 'Active');

INSERT INTO USER(username, password, first_name, last_name, email, state)
VALUES ('jMota','1234', 'Juan','Mota','jMota@xyz.com', 'Active');
 
-- Populate JOIN Table --
/*!40000 ALTER TABLE user_profile DISABLE KEYS */;
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_1.id, profile_1.id FROM user user_1, profile profile_1  
  where user_1.username='sduran' and profile_1.key_type='DEVE';
 
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_2.id, profile_1.id FROM user user_2, profile profile_1
  where user_2.username='fperez' and profile_1.key_type='DEVE';
 

INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_3.id, profile_2.id FROM user user_3, profile profile_2
  where user_3.username='garellano' and profile_2.key_type='ADMI';
 
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_4.id, profile_3.id FROM user user_4, profile profile_3
  where user_4.username='agutierrez' and profile_3.key_type='TEST';
 
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_5.id, profile_2.id FROM user user_5, profile profile_2  
  where user_5.username='jduran' and profile_2.key_type='ADMI';
 
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_5.id, profile_3.id FROM user user_5, profile profile_3  
  where user_5.username='jduran' and profile_3.key_type='TEST';
 
INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_6.id, profile_4.id FROM user user_6, profile profile_4  
  where user_6.username='jMota' and profile_4.key_type='USER';
 
 INSERT INTO USER_PROFILE (user_id, profile_id)
  SELECT user_7.id, profile_2.id FROM user user_7, profile profile_2
  where user_7.username='cperez' and profile_2.key_type='ADMI';
commit;