package com.iwa.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.iwa.converter.RoleToProfileConverter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.iwa")
public class AppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	RoleToProfileConverter roleToProfileConverter;

	/**
	 * set view resolvers to show views.
	 */
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {

		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
	}

	/**
	 * set resource handlers to get static resources (CSS, Javascript, etc)
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations(
				"/static/");
	}

	/**
	 * set converter to be used when convert string values[Roles] to Profiles in newUser.jsp file
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(roleToProfileConverter);
	}

	/**
	 * set message source to get validation and error messages stored in properties files
	 */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	/**
	 * Below code is only required when handling '.' in @PathVariables which otherwise ignore everything after last '.' in * @PathVaidables argument. 
	 * https://jira.spring.io/browse/SPR-6164, present in Spring 4.1.7.
	 */
	@Override
	public void configurePathMatch(PathMatchConfigurer matcher) {
		matcher.setUseRegisteredSuffixPatternMatch(true);
	}
}
