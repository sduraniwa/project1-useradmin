package com.iwa.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.iwa.model.Profile;
import com.iwa.service.ProfileService;

/**
 * Map id's to Profile objects.
 */
@Component
public class RoleToProfileConverter implements Converter<Object, Profile> {

	@Autowired
	ProfileService profileService;

	/**
	 * Gets Profile object providing id vaue
	 */
	public Profile convert(Object element) {
		Integer id = Integer.parseInt((String) element);
		Profile profile = profileService.findById(id);
		System.out.println("Profile : " + profile);
		return profile;
	}

}
