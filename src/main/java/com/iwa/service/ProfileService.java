package com.iwa.service;

import java.util.List;

import com.iwa.model.Profile;

public interface ProfileService {

	Profile findById(int id);

	Profile findByKeyType(String keyType);

	List<Profile> findAll();
}
