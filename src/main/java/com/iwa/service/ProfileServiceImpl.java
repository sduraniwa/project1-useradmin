package com.iwa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iwa.dao.ProfileDao;
import com.iwa.model.Profile;

@Service("profileService")
@Transactional
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	ProfileDao dao;

	public Profile findById(int id) {
		return dao.findById(id);
	}

	public Profile findByKeyType(String keyType) {
		return dao.findByKeyType(keyType);
	}

	public List<Profile> findAll() {
		return dao.findAll();
	}
}
