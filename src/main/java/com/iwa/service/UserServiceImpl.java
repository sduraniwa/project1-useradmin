package com.iwa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 

import com.iwa.dao.UserDao;
import com.iwa.model.User;
import com.iwa.service.UserService;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
    private UserDao dao;
 
    public User findById(int id) {
        return dao.findById(id);
    }
 
    public User findByUsername(String username) {
        User user = dao.findByUsername(username);
        return user;
    }
 
    public void saveUser(User user) {
        dao.save(user);
    }
 
    public void updateUser(User user) {
        User entity = dao.findById(user.getId());
        if(entity!=null){
            entity.setUsername(user.getUsername());
            entity.setPassword(user.getPassword());
            entity.setFirstName(user.getFirstName());
            entity.setLastName(user.getLastName());
            entity.setEmail(user.getEmail());
            entity.setUserProfiles(user.getUserProfiles());
        }
    }
 
     
    public void deleteUserByUsername(String username) {
        dao.deleteByUsername(username);
    }
 
    public List<User> findAllUsers() {
        return dao.findAllUsers();
    }
    
    public List<User> findAllUsers(Long page) {
        return dao.findAllUsers(page);
    }
 
    public boolean isUserUsernameUnique(Integer id, String username) {
        User user = findByUsername(username);
        return ( user == null || ((id != null) && (user.getId() == id)));
    }
}
