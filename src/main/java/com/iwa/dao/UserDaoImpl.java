package com.iwa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.iwa.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public User findById(int id) {
		User user = getByKey(id);
		if (user != null) {
			Hibernate.initialize(user.getUserProfiles());
		}
		return user;
	}

	public User findByUsername(String username) {
		System.out.println("Username : " + username);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User) crit.uniqueResult();
		if (user != null) {
			Hibernate.initialize(user.getUserProfiles());
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("firstName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		List<User> users = (List<User>) criteria.list();

		for (User user : users) {
			Hibernate.initialize(user.getUserProfiles());
		}

		return users;
	}

	private static final int limitResultsPerPage = 2;

	@SuppressWarnings("unchecked")
	public List<User> findAllUsers(Long page) {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("firstName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.setFirstResult((page.intValue() - 1) * limitResultsPerPage);
		criteria.setMaxResults(limitResultsPerPage);
		List<User> users = (List<User>) criteria.list();

		for (User user : users) {
			Hibernate.initialize(user.getUserProfiles());
		}

		return users;
	}

	public void save(User user) {
		persist(user);
	}

	public void deleteByUsername(String username) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User) crit.uniqueResult();
		delete(user);
	}
}
