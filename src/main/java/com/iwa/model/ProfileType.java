package com.iwa.model;

public enum ProfileType {
	ADMI("ADMI"), DEVE("DEVE"), TEST("TEST");

	String profileType;

	private ProfileType(String profileType) {
		this.profileType = profileType;
	}

	public String getProfileType() {
		return profileType;
	}

}
