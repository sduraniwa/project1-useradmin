<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users List</title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<div class="generic-container">
		<div class="panel panel-default">

			<div class="panel-heading" align="right">
				Hi <strong>${myUser}</strong>, Welcome to admin page. -  <a
					href="<c:url value="/logout" />">Logout</a>

			</div>


			<!-- Default panel contents -->
			<div class="panel-heading">
				<span class="lead">List of Users </span>

			</div>


			<table class="table table-hover">
				<thead>
					<tr>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Email</th>
						<th>Username</th>
						<th>Profile</th>
						<th width="100"></th>
						<th width="100"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users}" var="user">
						<tr>
							<td>${user.firstName}</td>
							<td>${user.lastName}</td>
							<td>${user.email}</td>
							<td>${user.username}</td>
							<td><c:forEach var="theUser" items="${user.userProfiles}">
									<form:option value="${theUser.id}">
										<c:out value="${theUser.keyType}" />
									</form:option>
								</c:forEach></td>
							<td><a href="<c:url value='/edit-user-${user.username}' />"
								class="btn btn-primary 
 
custom-width">edit</a></td>
							<td><a
								href="<c:url value='/delete-user-${user.username}' />"
								class="btn btn-danger 
 
custom-width">delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagination">
				<ul>
					<li><c:forEach begin="${startPage}" end="${endPage}" var="p">
							<a
								href="<c:url value="/list" ><c:param name="page" value="${p}"/>${p}</c:url>">${p}</a>
						</c:forEach></li>
				</ul>
			</div>
		</div>
		<div class="well">
			<a href="<c:url value='/newuser' />">Add New User</a>
		</div>
	</div>
</body>
</html>